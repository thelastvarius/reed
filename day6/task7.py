#coding=utf-8

from task6 import Student, Course
from datetime import date

class Teacher(object):
    def __init__(self, full_name, date, teacher_number):
        self.teacher_number = teacher_number
        self.full_name = full_name
        self.date = date

    def __str__(self):
        return "Lecturer. ID: {}; Name: {}; Birthday: {}. Course - {}\n".format(self.teacher_number, self.full_name, self.date, self.course)


    def set_course(self, course):
        self.course = course.name
        course.lecturer = self.full_name

class Course(Course):
    def __init__(self, name, lecture_hall):
        self.name = name
        self.lecture_hall = lecture_hall
        self.students = []
        self.lecturer = None

    def set_lecturer(self,lecturer):
        lecturer.set_lecturer(self)

    def __str__(self):
        result = "Course name: {}; Lecture hall: {}. Lecturer: {}. Students - {}:\n".format(self.name, self.lecture_hall, self.lecturer, len(self.students))
        for i in self.students:
            result += "ID: {}; Name: {}; Birthday: {}\n".format(i.student_number, i.full_name, i.date)
        return result

#petrov = Student('Petrov Petr Petrovich', date(1990, 1, 1), 1)
#ivanov = Student('Ivanov Ivan Ivanovich', date(1990, 2, 2), 2)
#rassadnikov = Student('Rassadnikov Nikita Pavlovich', date(1991, 7, 20), 3)
#sidorov = Teacher('Sidorov Sidr Sidorovich', date(1956, 3, 3), 1)
#ZI = Course('Information Security', 323)
#math = Course('Mathematic', 300)
#optic = Course('Optica', 521)
#sidorov.set_course(math)
#petrov.add_course(math)
#rassadnikov.add_course(ZI)
#rassadnikov.add_course(math)
#ivanov.add_course(optic)

#print(petrov)
#print(ivanov)
#print(rassadnikov)
#print(sidorov)
#print(ZI)
#print(math)