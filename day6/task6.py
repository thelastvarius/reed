#coding=utf-8

from datetime import date


class Student(object):
    def __init__(self, full_name, date, student_number):
        self.full_name = full_name
        self.date = date
        self.student_number = student_number
        self.course = []

    def __str__(self):
        result = "Student. ID: {}; Name: {}; Birthday: {}. Courses - {}:\n".format(self.student_number, self.full_name, self.date, len(self.course)) 
        for i in self.course:
            result += "Course name: {}; Lecture hall: {}\n".format(i.name, i.lecture_hall)
        return result

    def add_course(self, course):
        if not course in self.course:
            self.course.append(course)
            course.students.append(self)


class Course(object):
    def __init__(self, name, lecture_hall):
        self.name = name
        self.lecture_hall = lecture_hall
        self.students = []

    def __str__(self):
        result = "Course name: {}; Lecture hall: {}. Students - {}:\n".format(self.name, self.lecture_hall, len(self.students))
        for i in self.students:
            result += "ID: {}; Name: {}; Birthday: {}\n".format(i.student_number, i.full_name, i.date)
        return result

    def add_student(self, student):
        if not student in self.students:
            student.add_course(self)

#petrov = Student('Petrov Petr Petrovich', date(1990, 1, 1), 1)
#ivanov = Student('Ivanov Ivan Ivanovich', date(1990, 2, 2), 2)
#rassadnikov = Student('Rassadnikov Nikita Pavlovich', date(1991, 7, 20), 3)
#ZI = Course('Information Security', 323)
#math = Course('Mathematic', 300)
#optic = Course('Optica', 521)

#petrov.add_course(math)
#rassadnikov.add_course(ZI)
#rassadnikov.add_course(math)
#ivanov.add_course(optic)

#print(petrov)
#print(ivanov)
#print(rassadnikov)
#print(ZI)
#print(math)