#coding=utf-8

from task2 import Rectangle
from task1 import Point

class Rectangle3(Rectangle):
    def __init__(self, point1, point2):
        self.x1 = point1.x
        self.y1 = point1.y
        self.x2 = point2.x
        self.y2 = point2.y

point1 = Point(1,5)
point2 = Point(3,1)
test = Rectangle3(point1, point2)
print(test)
print test.sides()
print test.perimeter()
print test.area()
print test.square()