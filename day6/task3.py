#coding=utf-8

from task2 import Rectangle

class Rectangle2(Rectangle):
    def __init__ (self, x1, y1, lenght, width):
        self.x1 = x1
        self.y1 = y1
        self.lenght = lenght
        self.width = width
        self.x2 = self.x1 + self.lenght
        self.y2 = self.y1 - self.width

        if self.lenght < 0 or self.width < 0:
            raise ValueError("Lenght and width can't be negative")

    def __str__(self):
        return "Rectangle at {}, {}; lenght: {}, width: {}".format(self.x1, self.y1, self.lenght, self.width)

#test = Rectangle2(1, 5, 5, 3)
#print(test)
#print test.sides()
#print test.perimeter()
#print test.area()
#print test.square()