#coding=utf-8

class Rectangle(object):
    def __init__(self, x1, y1, x2, y2):
        self.x1 = x1
        self.y1 = y1
        self.x2 = x2
        self.y2 = y2

    def __str__(self):
        return "Rectangle at ({},{}), ({},{})".format(self.x1, self.y1, self.x2, self.y2)

    def sides(self):
        self.length = self.x2 - self.x1
        self.width = self.y1 - self.y2
        return "Length of the rectangle is {}. Width of the rectangle is {}.".format(self.length, self.width)

    def perimeter(self):
        return "Perimeter of the rectangle is {}".format(2*(self.length+self.width))

    def area(self):
        return "Area of the rectangle is {}".format(self.length*self.width)

    def square(self):
        if self.length == self.width:
            return True
        else:
            return False

#test = Rectangle(1, 5, 3, 2)
#print(test)
#print test.sides()
#print test.perimeter()
#print test.area()
#print test.square()