#coding=utf-8

class Circle(object):
    def __init__(self, center_x, center_y, radius):
        self.center_x = center_x
        self.center_y = center_y
        self.radius = radius

    def __str__(self):
        return "Сircle with center in ({}, {}) and radius {}".format(self.center_x, self.center_y, self.radius)

    def diameter(self):
        self.diameter = self.radius * 2
        return "Diameter of a circle is {}".format(self.diameter)

    def lenth(self):
        self.lenth = 2 * 3.14 * self.radius
        return "Length of a circle is {}".format(self.lenth)

    def area(self):
        self.area = 3.14 * (self.radius ** 2)
        return "Area of a circle is {}".format(self.area)

test = Circle(1, 5, 3)
print(test)
print test.diameter()
print test.lenth()
print test.area()