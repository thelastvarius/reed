#!coding=utf-8

from django.db import models
from datetime import datetime
from django.utils import timezone


class News(models.Model):
    news_header = models.CharField(u'Заголовок новости', max_length=100)
    news_text = models.CharField(u'Текст новости', max_length=1000)
    news_url = models.SlugField(u'url', max_length=50)
    news_create = models.DateTimeField(u'Дата создания', auto_now=False, default=timezone.now())
    news_changed = models.DateTimeField(u'Дата изменения', auto_now=True, auto_now_add=False)
    news_views = models.IntegerField(u'Количество просмотров', default=0)
    
    class Meta:
        verbose_name = u'Новость'
        verbose_name_plural = u'Новости'

    def __unicode__(self):
        return self.news_header


class User(models.Model):
    user_name = models.CharField(u'Имя', max_length=50)
    user_surname = models.CharField(u'Фамилия', max_length=100, blank=True)
    user_register = models.DateTimeField(u'Дата регистрации', auto_now=False, auto_now_add=True)

    class Meta:
        verbose_name = u'Пользователь'
        verbose_name_plural = u'Пользователи'

    def __unicode__(self):
        return '{} {}'.format(self.user_name, self.user_surname)


class Comment(models.Model):
    comment_text = models.CharField(u'Текст комментария', max_length=500)
    comment_register = models.DateTimeField(u'Дата публикации', auto_now=False, auto_now_add=True)
    news = models.ForeignKey(News, null=True, related_name='comment')
    user_name = models.CharField(u'Имя пользователя', max_length=50, blank=True)
    communication = models.CharField(u'Способ связи', max_length=50, blank=True)

    class Meta:
        verbose_name = u'Комментарий'
        verbose_name_plural = u'Комментарии'