"""august18 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin

from .views import main_view, date, weekday_by_date, date_by_weekday, day_between, pit, pension, insurance, site_main_page, news_list, news, random_news, best_news, new_news, top_news, register, comment, about, news_header, login_user, logout_user, profile_user

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^date/today', date),
    url(r'^weekday-by-date/(?P<date>[\d-]+)$', weekday_by_date),
    url(r'^date-by-weekday/(?P<day>[a-z]+)$', date_by_weekday),
    url(r'^date-range/(?P<date_begin>[\d-]+)/(?P<date_end>[\d-]+)', day_between),
    url(r'^pit/(?P<salary>[\d\.]+)', pit),
    url(r'^pension/(?P<salary>[\d\.]+)', pension),
    url(r'^insurance/(?P<salary>[\d\.]+)', insurance),
    url(r'^site/$', site_main_page),
    url(r'^news-list/$', news_list),
    url(r'^news/([\w-]+)/$', news),
    url(r'^random-news$', random_news),
    url(r'^best-news$', best_news),
    url(r'^new-news$', new_news),
    url(r'^top-news$', top_news),
    url(r'^site/register$', register),
    url(r'^add-comment$', comment),
    url(r'^about/$', about),
    url(r'^news-header/$', news_header),
    url(r'^login$', login_user),
    url(r'^logout$', logout_user),
    url(r'^profile$', profile_user)
]
