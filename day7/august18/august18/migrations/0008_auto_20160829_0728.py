# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2016-08-29 07:28
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('august18', '0007_auto_20160829_0645'),
    ]

    operations = [
        migrations.AlterField(
            model_name='news',
            name='news_create',
            field=models.DateTimeField(default=datetime.datetime(2016, 8, 29, 7, 28, 15, 985484, tzinfo=utc), verbose_name='\u0414\u0430\u0442\u0430 \u0441\u043e\u0437\u0434\u0430\u043d\u0438\u044f'),
        ),
        migrations.AlterField(
            model_name='news',
            name='news_header',
            field=models.CharField(max_length=100, verbose_name='\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a \u043d\u043e\u0432\u043e\u0441\u0442\u0438'),
        ),
        migrations.AlterField(
            model_name='news',
            name='news_text',
            field=models.CharField(max_length=1000, verbose_name='\u0422\u0435\u043a\u0441\u0442 \u043d\u043e\u0432\u043e\u0441\u0442\u0438'),
        ),
    ]
