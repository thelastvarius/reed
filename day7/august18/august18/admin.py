from django.contrib import admin
from .models import News, User, Comment


class ArticleAdmin(admin.ModelAdmin):
    prepopulated_fields = {'news_url': ('news_header',)}

admin.site.register(News, ArticleAdmin)
admin.site.register(User)
admin.site.register(Comment)
