#!coding=utf-8

from django import forms
from .models import Comment
import re


class RegisterForm(forms.Form):
    name = forms.CharField(label=u'Имя пользователя', max_length=20, required=True)
    phone = forms.CharField(label=u'Номер телефона', max_length=20, required=True)
    email = forms.CharField(label=u'Почта', max_length=30, required=False)
    password = forms.CharField(label=u'Пароль', max_length=20, required=True)

    def clean_phone(self):
        phone = self.cleaned_data['phone']
        clear_phone = clean_the_phone(phone)
        if not clear_phone:
            raise forms.ValidationError("Номер неверен. Проверьте правильность написания.")
        return clear_phone

    def clean_email(self):
        email = self.cleaned_data['email']
        clear_email = clean_the_email(email)
        if not clear_email:
            raise forms.ValidationError("Email неверен. Проверьте правильность написания.")
        return clear_email.group()

    def clean_password(self):
        password = self.cleaned_data['password']
        clear_password = clean_the_password(password)
        if not clear_password:
            raise forms.ValidationError("Пароль должен содержать строчные и прописные латинские буквы и цифры, а также быть не короче 6 символов.")
        return clear_password


def clean_the_phone(phone):
    result = re.sub(r"(\D)", r'', phone)
    return result[1:]

def clean_the_email(email):
    return re.search(r'^([\w\.\+-]+)@([\w\.\+-]+)\.(com|ru|net)$', email)

def clean_the_password(password):
    return re.search(r'^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}$', password)


class LoginForm(forms.Form):
    login = forms.CharField(label=u'Логин', max_length=20, required=True)
    password = forms.CharField(label=u'Пароль', max_length=30, required=True)


class CommentsForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ['comment_text', 'user_name', 'news']

    phone = forms.CharField(label=u'Номер телефона', required=False)
    email = forms.CharField(label=u'Электронная почта', required=False)


    def clean_phone(self):
        phone = self.cleaned_data['phone']
        print phone
        clear_phone = clean_the_phone(phone)
        return clear_phone


    def save(self):
        comment = super(CommentsForm, self).save(commit=False)
        comment.news = self.cleaned_data[u'news']
        comment.communication = self.cleaned_data[u'email'] or self.cleaned_data[u'phone']
        comment.user_name = self.cleaned_data[u'user_name']
        comment.save()
        return comment


    def clean(self):
        super(CommentsForm, self).clean()
        print self.cleaned_data
        if not self.cleaned_data['phone'] and not (self.cleaned_data['email']):
            raise forms.ValidationError("Одно из полей, номер или почта, должно быть заполнено")
        return self.cleaned_data
