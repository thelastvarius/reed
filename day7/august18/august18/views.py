#!coding=utf-8

import datetime
import locale
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render_to_response, render
from django.db.models import Count
from django.http import HttpResponseRedirect
from django.utils import timezone
from .models import News
from .forms import RegisterForm, CommentsForm, LoginForm
from pytils import dt
from random import randint
from loremipsum import get_sentence, get_paragraph



def main_view(request):
    context = {
        u'now':datetime.now(),
        u'numbers':(x for x in xrange(100)),
        u'tapki':Tapki.objects.all(),
    }

    return render_to_response(u'base.html', context)

def date(request):
    context = {u'date_today':datetime.date.today()}
    return render_to_response(u'date.html', context)

def weekday_by_date(request, date):
    context = {u'weekday':(dt.ru_strftime(u'%A', datetime.datetime.strptime(date, "%d-%m-%Y"))).capitalize()}
    return render_to_response(u'weekday_by_date.html', context)

def date_by_weekday(request, day):
    days = {'monday':1, 'tuesday':2, 'wednesday':3, 'thursday':4, 'friday':5, 'saturday':6, 'sunday':7}
    delta = days.get(day)-int(datetime.datetime.today().strftime("%w"))
    context = {u'date_by_weekday':(datetime.date.today()+datetime.timedelta(delta)).strftime("%d.%m.%Y")}
    return render_to_response(u'date_by_weekday.html', context)

def day_between(request, **kwargs):
    context = {u'day_between':abs((datetime.datetime.strptime(kwargs.get('date_begin'), "%Y-%m-%d")-datetime.datetime.strptime(kwargs.get('date_end'), "%Y-%m-%d")).days)}
    return render_to_response(u'day_between.html', context)

def pit(request, salary):
    context = {u'pit':float(salary)*0.13}
    return render_to_response(u'pit.html', context)

def pension(request, salary):
    context = {u'pension':float(salary)*0.22}
    return render_to_response(u'pension.html', context)

def insurance(request, salary):
    context = {
        u'pension':float(salary)*0.22,
        u'social_insurance':float(salary)*0.029,
        u'medical_insurance':float(salary)*0.051,
        u'accident_insurance':float(salary)*0.002,
        u'sum':float(salary)*0.302
    }

    return render_to_response(u'insurance.html', context)

def site_main_page(request):
    context = {u'date': datetime.datetime.now(), u'user':request.user}
    return render_to_response(u'index.html', context)

def news_list(request):
    context = {u'news':News.objects.all(), u'date':datetime.datetime.now(), u'user':request.user}
    return render_to_response(u'news_list.html', context)

def news(request,offset):
    news = News.objects.get(news_url=offset)
    news.news_views += 1
    news.save()
    print news.news_views
    context = {u'news':news, u'date':datetime.datetime.now(), u'user':request.user}
    return render_to_response(u'news.html', context)

def random_news(request):
    delta = randint(0,14)
    date = datetime.datetime.now() - datetime.timedelta(days=delta)
    news = News(news_create=date)
    news.news_header = get_sentence()
    news.news_text = get_paragraph()
    news.news_url = '{}_{}'.format(news.news_header.split()[0], news.news_header.split()[1])
    news.save()
    context = {u'news': news}
    return render_to_response(u'random_news.html', context)

def best_news(request):
    context = {u'news':News.objects.order_by('-news_views')[:10], u'date':datetime.datetime.now(), u'user':request.user}
    return render_to_response(u'best_news.html', context)

def new_news(request):
    context = {u'news':News.objects.order_by('-news_create')[:10], u'date':datetime.datetime.now(), u'user':request.user}
    return render_to_response(u'best_news.html', context)

def top_news(request):
    context = {u'news':News.objects.annotate(number=Count('comment')).order_by('-number')[:10], u'date':datetime.datetime.now(), u'user':request.user}
    return render_to_response(u'best_news.html', context)

def register(request):
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            return HttpResponseRedirect('/site')
    else:
        form = RegisterForm()

    context = {u'form':form}
    return render(request, u'register.html', context)

def comment(request):
    context = {}
    if request.method == 'POST':
        form = CommentsForm(request.POST)
        if form.is_valid():
            context[u'success'] = True
            form.save()
    else:
        form = CommentsForm()
    context[u'form'] = form
    print context
    return render(request, u'comment.html', context)

def about(request):
    context = {u'date': datetime.datetime.now(), u'date':datetime.datetime.now(), u'user':request.user}
    return render_to_response(u'about.html', context)

def news_header(request):
    context = {u'news':News.objects.all(), u'new':(1,2,3,4), u'mid':len(News.objects.all())/2}
    return render_to_response(u'news_header.html', context)

def login_user(request):
    context = {}
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            context[u'success'] = True
            user = authenticate(
                username = form.cleaned_data.get('login'),
                password = form.cleaned_data.get('password')
                )
            if user is not None:
                if user.is_active:
                    login(request, user)
                    return HttpResponseRedirect('/site')
    else:
        form = LoginForm()
    context[u'form'] = form
    return render(request, u'login.html', context)

def logout_user(request):
    logout(request)
    return HttpResponseRedirect('/site')

def profile_user(request):
    time_from_registration = (timezone.now() - request.user.date_joined).days
    context = {u'time':time_from_registration, u'user':request.user}
    return render_to_response(u'profile.html', context)
