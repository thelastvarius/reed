#!coding=utf-8
import re

class Checker(object):
    def __init__(self, password):
        self.password = password

    def inspect(self, expression):
        return re.search(expression, self.password) is not None

    def inspect_4_numbers(self):
        return self.inspect(r'^\d{4,}$')

    def inspect_6_10_alpha(self):
        return self.inspect(r'^[a-z]{4,10}$')

    def inspect_numbers_alpha_symbols(self):
        return self.inspect(r'^\w[@._-]{6,10}$')

    def inspect_all_symbols(self):
        return self.inspect(r'^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@._-]).{6,10}$')


def phone_number(phone):
    result = re.sub(r"(\D)", r'', phone)
    return result[1:]

def email(email):
    return re.search(r'^([\w\.\+-]+)@([\w\.\+-]+)\.(com|ru|net)$', email) is not None


class Fruit(object):
    def __init__(self, text):
        self.text = text

    def count(self, frutty):
        res = 0
        for element in re.findall(frutty, self.text):
            res += int(element)
        return res


    def count_apple(self, fruit):
        return self.count(r'{}: (\d)+'.format(fruit))

    def count_pear(self, fruit):
        return self.count(r'{}: (\d)+'.format(fruit))

    def count_melon(self, fruit):
        return self.count(r'{}: (\d)+'.format(fruit))

    def summ_fruits(self):
        return sum(map(int, re.findall(r'\d+', self.text)))

    def summary(self):
        fruits = re.findall(r' (\w+): ', self.text.decode('utf-8'), re.U)
        dic = {element.encode('utf-8'):self.count(r'{}: (\d)+'.format(element.encode('utf-8'))) for element in fruits}
#   Так как при возвращении словаря наблюдаются небольшие проблемы с кодировкой - тут оставлен вывод элементов через принт.
#        for x,y in dic.iteritems():
#            print '{}:{}'.format(x, y)
        return dic 

list_password = ['123qweRT@', '123', '1234', 'qwerty', '123qwert']
for element in list_password:
    test_pass = Checker(element)
    print 'Проверка пароля {}:'.format(test_pass.password)
    print 'Только цифры, 4 символа и более - {}'.format(test_pass.inspect_4_numbers())
    print 'Только латинские строчные, от 6 до 10 сиволов - {}'.format(test_pass.inspect_6_10_alpha())
    print 'Латинские строчные, прописные, цифры и спецсимволы, от 6 до 10 символов - {}'.format(test_pass.inspect_numbers_alpha_symbols())
    print 'Предыдущий тест с проверкой наличия всех групп символов - {}'.format(test_pass.inspect_all_symbols())
    print 'Проверка окончена'
print '\n'
test = Fruit('Яна съела яблок: 5. Аня съела яблок: 4. Андрей съел дынь: 5. Сергей съел яблок: 2.')
print 'Проверка класса фрукты:'
print test.count_apple('яблок')
print test.count_pear('груш')
print test.count_melon('дынь')
print test.summ_fruits()
print test.summary()
print 'Проверка email'
print email('Test@test.com')