def lol(list):
    if len(list) == 0:
        return 0
    else:
        return sum(list[::2])*list[-1]

print lol([0,1,2,3,4,5])
print lol([1,3,5])
print lol([6])
print lol([])