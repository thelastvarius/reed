def simple():
    x = 0
    res = 0
    for number in range(2,301):
        for i in range(1,number+1):
            if number % i == 0:
                x += 1
        if x == 2:
            res += 1
        x = 0
    return res

print simple()