def check(string):
    i = 0;
    l = string.split()
    for element in l:
        if element.isalpha():
            i += 1
        else:
            i = 0
    if i >= 3:
        return True
    else: 
        return False

print check("Hello World hello")
print check("He is 123 man")
print check("1 2 3 4")
print check("bla bla bla bla")
print check("Hi")