#coding=utf-8

import math

def const():
    return 2

def involution(number):
    return number**3

def factorial(number):
    return math.factorial(number)

def minimal(number1, number2):
    return min(number1,number2)

def minimal_len(number1, number2):
    dict = {len(str(number1)):number1, len(str(number2)):number2}
    return dict[min(dict.keys())]

def hypotenuse(number1, number2):
    return math.hypot(number1,number2)

def last_numeral(number):
    if number > 0 and number % 1 == 0:
        return int(str(number)[-1])
    else:
        return 'Не натуральное число'

def summ_numeral(number):
    if len(str(number)) == 3:
        summ = 0
        for x in str(number):
            summ += int(x)
        return summ
    else:
        return 'Не трехзначное число'

def summ_numeral_all(number):
    summ = 0
    for x in str(number):
        summ += int(x)
    return summ

def selection(list):
    return list.count(2)

def selection2(list, number):
    return list.count(number)

def selection3(list, number=2):
    return list.count(number)
            
def selection4(list, number=2):
    return list.count(number)

def selection5(*args):
    l = 0
    res = ''
    for x in args:
        if len(x) > l:
            l = len(x)
            res = x
    return res

def selection6(*args):
    l = 0
    res = ''
    for x in args:
        if len(x) > l:
            l = len(x)
            res = x
    return res, l

def year(number):
    if number % 4 == 0:
        if number % 100 == 0:
            if number % 400 == 0:
                return True
            else:
                return False
        else: 
            return True
    else:
        return False

def shift(list):
    return list[-1:] + list[:-1]

print const()
print involution(3)
print factorial(5)
print minimal(5,3)
print minimal_len(5, 26)
print hypotenuse(2,3)
print last_numeral(57)
print summ_numeral(123)
print summ_numeral_all(123456789)
print selection([1,2,3,4,5])
print selection2([1,2,3,4,5,2,3,6,2],2)
print selection3([1,2,3,4,4])
print selection4([1,2,3,'1','2',[1,2]],'1')
print selection5('1','123','12345')
print selection6('1','123','12345')
print year(2016)
print shift([1,2,3,4,5])