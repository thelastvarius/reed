def find_difference(*args):
    if len(args) == 0:
        return 0
    else:
        return max(args)-min(args)

print find_difference(1,2,3)
print find_difference(5,-5)
print find_difference(10.2,-2.2,0,1.1,0.5)
print find_difference()