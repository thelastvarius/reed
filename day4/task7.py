import datetime

def days_diff(date1,date2):
    a = list(date1)
    b = list(date2)
    return int(abs((datetime.date(int(a[0]),int(a[1]),int(a[2])) - datetime.date(int(b[0]),int(b[1]),int(b[2]))).days))

print days_diff((1982, 4, 19), (1982, 4, 22))
print days_diff((2014, 1, 1), (2014, 8, 27))
print days_diff((2014, 8, 27), (2014, 1, 1))